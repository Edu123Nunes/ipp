<div style="text-align: justify">
**Cancro Bacteriano**</br>
*Xanthomonas campestris pv. viticola*</br>
O Cancro Bacteriano causado pela bactéria *Xanthomonas campestris*, tem como sintoma o surgimento de pontos necrótico com aparecimento de halo amarelo. </br>
Controle através do uso de material vegetativo certificado, assim como eliminar ramos infectados, além da desinfestação de ferramentas e implementos após seu uso.
