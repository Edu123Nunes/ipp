<div style="text-align: justify">
**Antracnose no Feijão**</br>
*Colletotrichum lindemuthianum*</br>
A antracnose no feijoeiro é causada pelo fungo *Colletotrichum lindemuthianum*, podendo causar perda total de uma área. Essa doença é favorecida por temperaturas baixas até moderadas, contudo com alta umidade.</br> 
Para seu controle, deve se usar sementes sadias, como também através da rotação de cultura na área e por fim o uso de cultivares de feijão resistentes. 