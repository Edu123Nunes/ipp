<div style="text-align: justify">
**Mofo Branco**</br>
*Sclerotinia sclerotiorum*</br>
O Mofo Branco é causado pelo fungo *Sclerotinia sclerotiorum*. Que causa a podridão da região do caule da planta. Por conta disso apresentar amarelecimento e seca de folhas. </br>
Para controle se recomenda a eliminação de restos culturais e a realização da rotação de culturas. 

