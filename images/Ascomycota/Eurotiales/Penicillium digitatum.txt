<div style="text-align: justify">
**Bolor verde no citros** </br> 
 *Penicillium digitatum*</br>
O fungo  *Penicillium digitatum*  causa a podridão de sementes e a morte de planta em seu início de vida. Consegue sobreviver tanto no solo ou no interior das sementes.
