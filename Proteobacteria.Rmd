```{r ,  include=FALSE}
source("in_img.R")
```
# Filo *Proteobacteria*
<div style="text-align: justify">
Estando sobre o domínio de Bactérias. Este é o filo com maior número de espécies sobre. Estas espécies são responsáveis por um grande volume de doenças que afetam não somente plantas, como também seres humanos, como o caso da Salmonela e gonorreia. 

## Ordem *Xanthomonadales*
<div style="text-align: justify">
 A ordem *Xanthomonadales* é uma das maiores ordens das Bactérias. Apresentando um grande número de fito patógenos. Também é encontrado em inoculantes tendo papel como microorganismos eficientes, tendo papel como, por exemplo, na quebra de dormência de sementes.
```{r, include=FALSE}
Xanthomonadales <- in_img(filo="Proteobacteria", ordem="Xanthomonadales")
```
`r paste(knit(text = Xanthomonadales))`

## Ordem *Pseudomonadales*
<div style="text-align: justify">
A ordem *Pseudomonadales*, é conhecido por ter agentes acarretando em infecções, importantes para a saúde do homem, como também em animais. Além de afetar plantas vasculares, seja de forma benéfica para controle de doenças(em estudo). Como também causar doenças.  

```{r, include=FALSE}
Pseudomonadales <- in_img(filo="Proteobacteria", ordem="Pseudomonadales")
```
`r paste(knit(text = Pseudomonadales))`


